﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;

namespace ChatInfrastructure
{
    public static class TcpClientExtensions
    {
        public static Message GetMessage(this TcpClient client)
        {
            try
            {
                byte[] bytes = new byte[client.ReceiveBufferSize];
                using (NetworkStream ns = new NetworkStream(client.Client, FileAccess.ReadWrite, false))
                {
                    int bytesNumber = ns.Read(bytes, 0, bytes.Length);
                    Message message = Message.MessageFromBytes(bytes);
                    return message;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public static void SendMessage(this TcpClient client, Message message)
        {
            try
            {
                byte[] bytes = Message.MessageToBytes(message);
                using (NetworkStream ns = new NetworkStream(client.Client, FileAccess.ReadWrite, false))
                {
                    ns.Write(bytes, 0, bytes.Length);
                    ns.Flush();//1
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
    }
}
