﻿using System.Net;

namespace ChatInfrastructure
{
    public static class Settings
    {
        public static IPAddress IpAddress { get; set; } = IPAddress.Parse("192.168.1.44");
        public static int Port { get; set; } = 8000;
    }
}
