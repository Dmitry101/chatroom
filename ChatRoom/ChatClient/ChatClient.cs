﻿using ChatInfrastructure;
using System;
using System.Net.Sockets;
using System.Threading;

namespace ChatClient
{
    public class ChatClient
    {
        private TcpClient client;
        private string userName;

        public ChatClient()
        {
            client = new TcpClient(Settings.IpAddress.ToString(), Settings.Port);

            Register();
            Authorize();
            StartChat();
        }

        private void StartChat()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(GetMessages));
            SendMessages();
        }

        private void SendMessages()
        {
            string text = "";
            while ((text = Console.ReadLine()).ToLower() != "exit")
            {
                if (client.Connected)
                {
                    Message message = new Message(userName, MessageType.Text, text);
                    client.SendMessage(message);
                }
                else
                {
                    break;
                }
            }
        }

        private void GetMessages(object state)
        {
            while (true)
            {
                if (client.Connected)
                {
                    try
                    {
                        Message message = client.GetMessage();
                        if (message != null)
                        {
                            Console.WriteLine(message.ToString());
                        }
                    }
                    catch
                    {
                        //2
                    }
                }
                else
                {
                    Console.WriteLine("*** Connection is lost...");
                    break;
                }
            }
        }

        private void Authorize()
        {
            Message message = new Message(userName, MessageType.Info);
            client.SendMessage(message);
        }

        private void Register()
        {
            Console.WriteLine("Enter your name: ");
            userName = Console.ReadLine();
        }
    }
}
