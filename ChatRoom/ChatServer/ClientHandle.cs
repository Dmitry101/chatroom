﻿using ChatInfrastructure;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

namespace ChatServer
{
    public class ClientHandler
    {
        private TcpClient client;

        public ClientHandler() { }

        public ClientHandler(TcpClient client)
        {
            this.client = client;
            string userName = GetNewUserName(client);
            bool clientAdded = ChatServer.AddNewClient(userName, client);
            while (!clientAdded)
            {
                userName = GetNewUserName(client);
                clientAdded = ChatServer.AddNewClient(userName, client);
            }

            ThreadPool.QueueUserWorkItem(new WaitCallback(StartChat));
        }

        private void StartChat(object state)
        {
            try
            {
                while (client.Connected)
                {
                    Message message = client.GetMessage();
                    ChatServer.Broadcast(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
            }
        }

        private string GetNewUserName(TcpClient client)
        {
            Message message = client.GetMessage();
            if (message != null)
            {
                return message.Sender;
            }

            return null;
        }
    }
}
