﻿using ChatInfrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Linq;

namespace ChatServer
{
    public static class ChatServer
    {
        private static Dictionary<string, TcpClient> clients;
        private static readonly object locker = new object();
        private static TcpListener server;

        public static bool Stopped;

        public static void Start()
        {
            if (server != null) return;
            Stopped = false;
            try
            {
                server = new TcpListener(Settings.IpAddress, Settings.Port);
                server.Start();
                clients = new Dictionary<string, TcpClient>();
                Console.WriteLine("*** Chat Room opened...");
                while (!Stopped)
                {
                    if (server.Pending())
                    {
                        TcpClient client = server.AcceptTcpClient();
                        new ClientHandler(client);
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                Stop();
                Console.WriteLine("Exit");
                Console.ReadLine();
            }
        }

        public static void Stop()
        {
           if(server != null)
           {
                server.Stop();
                server = null;
                lock (locker)
                {
                    foreach (TcpClient client in clients.Values)
                    {
                        client.SendMessage(new Message("", MessageType.Info, "Sorry. Server was disconnected. Try to re-connect later."));
                        client.Close();
                    }

                    clients.Clear();
                    clients = null;
                }
           }
        }

        public static bool AddNewClient(string userName, TcpClient client)
        {
            userName = userName.ToLower();
            if (clients.ContainsKey(userName))
            {
                client.SendMessage(new Message("", MessageType.Info, "A user with the same name already exists. Enter another name."));
                return false;
            }
            else
            {
                lock (locker)
                {
                    clients.Add(userName, client);
                }

                SendContactsToNewUser(userName);
                ShowNewUser(userName);
                return true;
            }
        }

        private static string GetUsersList(string currentUserName)
        {
           return string.Join(", ", clients.Keys.Where(x => x != currentUserName).ToArray());
        }

        private static void SendContactsToNewUser(string userName)
        {
            string users = GetUsersList(userName);
            if (users != null && users.Length > 0)
            {
                Message message = new Message(userName, MessageType.Info, "users in this room:\n*** " + users);
                TcpClient client = clients[userName];
                if (client != null)
                {
                    client.SendMessage(message);
                }
            }
        }

        private static void ShowNewUser(string userName)
        {
            Console.WriteLine(new Message(userName, MessageType.Info, userName + " joined chat room").ToString());
        }

        public static void Broadcast(Message message)
        {
            Console.WriteLine(message.ToString());
            lock (locker)
            {
                foreach (KeyValuePair<string, TcpClient> c in clients)
                {
                    if (c.Key != message.Sender)
                    {
                        TcpClient client = c.Value;
                        if (client != null && client.Connected)
                        {
                            client.SendMessage(message);
                        }
                    }
                }
            }
        }
    }
}
