﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatUtils
{
    public static class BinariFormatterHelper<T>
        where T : class, new()
    {
        public static byte[] Serialize(T obj)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    formatter.Serialize(ms, obj);
                    return ms.ToArray();
                }
                catch
                {
                    return null;
                }
            }
        }

        public static T Deserialize(byte[] bytes)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                try
                {
                    ms.Position = 0;
                    return (formatter.Deserialize(ms) as T);
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
