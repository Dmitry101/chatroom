﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Delay(15000).ContinueWith(t =>
            {
                ChatServer.Stopped = true;
            });
            ChatServer.Start();
        }
    }
}
