﻿using System;
using ChatUtils;

namespace ChatInfrastructure
{
    public enum MessageType
    {
        Text,
        Info
    }

    [Serializable]
    public class Message
    {
        public static string TextPreffix { get; set; } = ">> ";
        public static string NotificationPreffix { get; set; } = "*** ";
        public string Sender { get; set; }
        public MessageType Type { get; set; }
        public string Text { get; set; }

        public Message() { }

        public Message(string sender) : this(sender, MessageType.Text, "") { }

        public Message(string sender, MessageType type) : this(sender, type, "") { }

        public Message(string sender, MessageType type, string text)
        {
            Sender = sender;
            Type = type;
            Text = text;
        }

        public static byte[] MessageToBytes(Message message)
        {
            return BinariFormatterHelper<Message>.Serialize(message);
        }

        public static Message MessageFromBytes(byte[] bytes)
        {
            return BinariFormatterHelper<Message>.Deserialize(bytes);
        }

        public override string ToString()
        {
            string resultString = "";
            switch (Type)
            {
                case MessageType.Text:
                    resultString = TextPreffix;
                    if (Sender != null && Sender.Length > 0)
                    {
                        resultString += Sender + ": ";
                    }
                    break;
                case MessageType.Info:
                    resultString = NotificationPreffix;
                    break;
                default:
                    break;
            }

            resultString += Text;
            return resultString;
        }
    }
}
